#!/usr/bin/python3
from math import log2
from os import path


def read_data():
    filename = "base_exp.txt"
    powers = []
    with open(path.join(path.dirname(path.realpath(__file__)), filename)) as f:
        for line in f.readlines():
            splited = line.rstrip().split(",")
            powers.append([int(s) for s in splited])
    return powers


def is_greatest_power(first, second):
    return log2(first[0])*first[1] > log2(second[0])*second[1]


def main():
    line = 0
    maximum = [1, 1]
    for i, power in enumerate(read_data()):
        if is_greatest_power(power, maximum):
            line = i + 1
            maximum = power[:]
    return line


if __name__ == '__main__':
    print(main())
