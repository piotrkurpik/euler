#!/usr/bin/python3

import unittest
from os import path
from pr099 import program


class Problem099TestCase(unittest.TestCase):
    answer_file = path.join(path.dirname(path.realpath(__file__)), 'answer')

    def test_compare_powers(self):
        result = program.is_greatest_power([2, 11], [3, 7])
        self.assertFalse(result)
        result = program.is_greatest_power([632382, 518061], [519432, 525806])
        self.assertTrue(result)

    @unittest.skipUnless(path.isfile(answer_file), "no answer file exists")
    def test_main(self):
        result = str(program.main())
        with open(self.answer_file) as a:
            expected = a.read().split("\n")[0]
        self.assertEqual(result, expected)


if __name__ == '__main__':
    unittest.main(verbosity=2)
