#!/usr/bin/python3
from eulerlib.numbers import gcd


def fraction_on_left(n, d, dmax):
    n_left, d_left = 1, dmax
    for i in range(2, dmax+1):
        j = int(n*i/d)
        if i*n > j*d and i*n_left < j*d_left:
            if gcd(i, j) == 1:
                n_left, d_left = j, i
    return n_left, d_left


def main():
    return fraction_on_left(3, 7, 1000000)[0]


if __name__ == '__main__':
    print(main())
