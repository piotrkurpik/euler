#!/usr/bin/python3
from itertools import count
from eulerlib.polygonals import polygonal


def polygonal_between(n, minimum, maximum):
    ps = []
    for i in count(1):
        p = polygonal(n, i)
        if p > maximum:
            return ps
        if p > minimum:
            ps.append(p)


def ordered_set_of_n_4digits_polygonals(n):
    polygonals = []
    for i in range(3, n+1):
        polygonals.append(polygonal_between(i, 1000, 10000))
    for i in polygonals[-1]:
        for solution in find_next_element(i, polygonals[:-1]):
            if solution[-1] % 100 == i//100:
                return solution


def find_next_element(last, numbers):
    if len(numbers) == 0:
        return [last]
    solutions = []
    for tab in numbers:
        tmp_numbers = [t for t in numbers if t != tab]
        for n in tab:
            if last % 100 == n//100:
                for s in find_next_element(n, tmp_numbers):
                    if type(s) == list:
                        solutions.append([last]+s)
                    else:
                        solutions.append([last]+[s])
    return solutions


def main():
    return sum(ordered_set_of_n_4digits_polygonals(8))


if __name__ == '__main__':
    print(main())
