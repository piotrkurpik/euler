#!/usr/bin/python3

import unittest
from os import path
from pr061 import program


class Problem061TestCase(unittest.TestCase):
    answer_file = path.join(path.dirname(path.realpath(__file__)), 'answer')

    def test_up_to_pentagonal(self):
        result = program.ordered_set_of_n_4digits_polygonals(5)
        expected = [2882, 8281, 8128]
        self.assertEqual(result, expected)

    @unittest.skipUnless(path.isfile(answer_file), "no answer file exists")
    def test_main(self):
        result = str(program.main())
        with open(self.answer_file) as a:
            expected = a.read().split("\n")[0]
        self.assertEqual(result, expected)


if __name__ == '__main__':
    unittest.main(verbosity=2)
