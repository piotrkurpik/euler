#!/usr/bin/python3
from itertools import count


def main():
    perfects = 0
    last_power_of_2 = 1
    for i in count(2):
        if i == 2*last_power_of_2:
            last_power_of_2 *= 2
            perfects += 1
        if 12345*perfects < i - 1:
            return i*i - i


if __name__ == '__main__':
    print(main())
