#!/usr/bin/python3
def square_laminaes_below(limit):
    count = 0
    for i in range(3, limit//4+2):
        for j in range(i-2, 0, -2):
            if i*i - j*j > limit:
                break
            count += 1
    return count


def main():
    return square_laminaes_below(10**6)


if __name__ == '__main__':
    print(main())
