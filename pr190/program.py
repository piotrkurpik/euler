#!/usr/bin/python3
def P(m):
    answer = 1
    for i in range(1, m+1):
        for _ in range(i):
            answer *= 2*i
            answer /= m+1
    return int(answer)


def main():
    return sum([P(m) for m in range(2, 16)])


if __name__ == '__main__':
    print(main())
