#!/usr/bin/python3

import unittest
from os import path
from pr086 import program


class Problem086TestCase(unittest.TestCase):
    answer_file = path.join(path.dirname(path.realpath(__file__)), 'answer')

    def test_exceeding_2000(self):
        result = program.number_of_cuboids_exceeds(2000)
        expected = 100
        self.assertEqual(result, expected)

    @unittest.skipUnless(path.isfile(answer_file), "no answer file exists")
    def test_main(self):
        result = str(program.main())
        with open(self.answer_file) as a:
            expected = a.read().split("\n")[0]
        self.assertEqual(result, expected)


if __name__ == '__main__':
    unittest.main(verbosity=2)
