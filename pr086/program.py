#!/usr/bin/python3
from itertools import count


def pythagorean_with_cathetus(n):
    for i in range(3, 2*n):
        c = (i*i+n*n)**0.5
        if c == int(c):
            yield (n, i, int(c))


def cuboids_with_c_equal_M(M):
    i = 0
    for a, b, c in pythagorean_with_cathetus(M):
        if b > a:
            i -= b-a-1
        i += b//2
    return i


def number_of_cuboids_exceeds(n):
    c = 0
    for i in count():
        c += cuboids_with_c_equal_M(i)
        if c > n:
            return(i)


def main():
    return number_of_cuboids_exceeds(1000000)


if __name__ == '__main__':
    print(main())
