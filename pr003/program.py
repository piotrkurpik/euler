#!/usr/bin/python3
def largest_prime_factor(n):
    d = 2
    while n % d == 0:
        n /= d
    if n == 1:
        return d
    d = 1
    while n > 1:
        d += 2
        while n % d == 0:
            n /= d
    return d


def main():
    number = 600851475143
    return largest_prime_factor(number)


if __name__ == '__main__':
    print(main())
