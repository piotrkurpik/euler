#!/usr/bin/python3
def max_remaider(a):
    #  we are looking for even n where 2an % a%2 is the greates
    return 2 * a * ((a-1)//2)


def main():
    s = 0
    for i in range(3, 1001):
        s += max_remaider(i)
    return s


if __name__ == '__main__':
    print(main())
