#!/usr/bin/python3
def main():
    digits = 2
    table = [[0]*9]
    for i in range(1, 10):
        table.append([1]*(10-i))
    while digits < 20:
        new_table = []
        for i in range(10):
            new_table.append([0]*(10-i))
        for i, row in enumerate(table):
            for j, numbers in enumerate(row):
                for k in range(10-i-j):
                    new_table[j][k] += table[i][j]
        table = [row[:] for row in new_table]
        digits += 1
    return sum([sum(row) for row in table])


if __name__ == '__main__':
    print(main())
