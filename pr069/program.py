#!/usr/bin/python3
from eulerlib.numbers import is_prime
from itertools import count


def value_wit_greatest_totiens_ratio_below(limit):
    # maximum is reached when we multiplied as many smallest primes as possible
    n = 2
    for i in count(3, 2):
        if is_prime(i):
            if n*i > limit:
                return n
            n *= i


def main():
    return value_wit_greatest_totiens_ratio_below(1000000)


if __name__ == '__main__':
    print(main())
