#!/usr/bin/python3

import unittest
from os import path
from pr075 import program


class Problem075TestCase(unittest.TestCase):
    answer_file = path.join(path.dirname(path.realpath(__file__)), 'answer')

    def test_small_pythagorean_triples(self):
        per = program.ways_to_form_triplets_with_perimeter_below(121)
        for i in [12, 24, 30, 36, 40, 48]:
            self.assertEqual(per[i], 1)
        self.assertEqual(per[20], 0)
        self.assertEqual(per[120], 3)

    @unittest.skipUnless(path.isfile(answer_file), "no answer file exists")
    def test_main(self):
        result = str(program.main())
        with open(self.answer_file) as a:
            expected = a.read().split("\n")[0]
        self.assertEqual(result, expected)


if __name__ == '__main__':
    unittest.main(verbosity=2)
