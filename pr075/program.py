#!/usr/bin/python3
from eulerlib.numbers import ordered_gcd


def primitive_pythagorean_triplets_w_perimeter_less(n):
    i = 2
    while 2*i*(i+1) <= n:
        j = (i % 2) + 1
        while j < i and 2*i*(i+j) <= n:
            if ordered_gcd(i, j) == 1:
                yield (i*i-j*j, 2*i*j, i*i+j*j)
            j += 2
        i += 1


def ways_to_form_triplets_with_perimeter_below(limit):
    perimeters = [0] * (limit+1)
    for a, b, c in primitive_pythagorean_triplets_w_perimeter_less(limit):
        for p in range(a+b+c, limit+1, a+b+c):
            perimeters[p] += 1
    return perimeters


def main():
    return sum(
        [p == 1 for p in ways_to_form_triplets_with_perimeter_below(1500000)]
    )


if __name__ == '__main__':
    print(main())
