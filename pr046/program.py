#!/usr/bin/python3
from eulerlib.numbers import is_prime
from math import sqrt
from itertools import count


def goldbach(n):
    if is_prime(n):
        return True
    for i in range(1, int(sqrt(n/2))+1):
        if is_prime(n-2*i*i):
            return True
    return False


def main():
    for i in count(9, 2):
        if not goldbach(i):
            return i


if __name__ == '__main__':
    print(main())
