#!/usr/bin/python3
def main():
    expotent = 1
    numbers = 0
    while 9**expotent > 10**(expotent-1):
        base = 9
        while base**expotent >= 10**(expotent-1):
            numbers += 1
            base -= 1
        expotent += 1
    return numbers


if __name__ == '__main__':
    print(main())
