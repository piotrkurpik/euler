#!/usr/bin/python3
from itertools import permutations


def main():
    products = []
    for i in permutations(range(1, 10)):
        s = "".join([str(j) for j in i])
        p = int(s[:4])
        for m in [5, 6]:
            if p == int(s[4:m]) * int(s[m:9]):
                if p not in products:
                    products.append(p)
    return sum(products)


if __name__ == '__main__':
    print(main())
