#!/usr/bin/python3
def throws_equaling(dices, sides, result):
    if result <= 0 or result > sides*dices:
        return 0
    if dices == 1:
        return 1
    throws = 0
    for i in range(1, sides+1):
        throws += throws_equaling(dices-1, sides, result-i)
    return throws


def main():
    d1, s1 = 9, 4
    d2, s2 = 6, 6
    result = 0
    for i in range(d1, d1*s1+1):
        for j in range(d2, i):
            result += throws_equaling(d1, s1, i)*throws_equaling(d2, s2, j)
    result /= (s1**d1)*(s2**d2)
    return "{0:0.7f}".format(result)


if __name__ == '__main__':
    print(main())
