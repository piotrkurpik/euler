#!/usr/bin/python3
from eulerlib.numbers import fibonnaci_below


def fibonaci_n_digits_long(n):
    return len(list(fibonnaci_below(10**(n-1)))) + 1


def main():
    return fibonaci_n_digits_long(1000)


if __name__ == '__main__':
    print(main())
