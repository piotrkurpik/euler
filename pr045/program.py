#!/usr/bin/python3
from eulerlib.polygonals import is_triangular, is_pentagonal, hexagonal
from itertools import count


def is_triangular_and_pentagonal(n):
    if not is_triangular(n):
        return False
    if not is_pentagonal(n):
        return False
    return True


def main():
    for i in count(144):  # because we look for next after 143th hexagonal
        n = hexagonal(i)
        if is_triangular_and_pentagonal(n):
            return n


if __name__ == '__main__':
    print(main())
