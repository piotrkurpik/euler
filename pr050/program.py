#!/usr/bin/python3
from eulerlib.numbers import sieve_of_eratosthenes


def longest_sum_of_consecutives_primes_below(n):
    primes = sieve_of_eratosthenes(n)
    longest, value = 0, 0
    i = 0
    while primes[i]*longest < n:
        j = i+longest+1
        primes_to_sum = primes[i:j]
        while sum(primes_to_sum) < n:
            primes_to_sum.append(primes[j])
            if sum(primes_to_sum) in primes:
                value = sum(primes_to_sum)
                longest = len(primes_to_sum)
            j += 1
        i += 1
    return value


def main():
    return longest_sum_of_consecutives_primes_below(1000000)


if __name__ == '__main__':
    print(main())
