#!/usr/bin/python3

import unittest
from os import path
from pr050 import program


class Problem050TestCase(unittest.TestCase):
    answer_file = path.join(path.dirname(path.realpath(__file__)), 'answer')

    def test_100(self):
        result = program.longest_sum_of_consecutives_primes_below(100)
        expected = 41
        self.assertEqual(result, expected)

    def test_1000(self):
        result = program.longest_sum_of_consecutives_primes_below(1000)
        expected = 953
        self.assertEqual(result, expected)

    @unittest.skipUnless(path.isfile(answer_file), "no answer file exists")
    def test_main(self):
        result = str(program.main())
        with open(self.answer_file) as a:
            expected = a.read().split("\n")[0]
        self.assertEqual(result, expected)


if __name__ == '__main__':
    unittest.main(verbosity=2)
