#!/usr/bin/python3
from eulerlib.polygonals import triangular_number


def sum_all_multiples_below(m, n):
    how_many = (n-1)//m
    return m*triangular_number(how_many)


def sum_multiples_of_3_and_5_below(n):
    result = sum_all_multiples_below(3, n) + \
             sum_all_multiples_below(5, n) - \
             sum_all_multiples_below(3*5, n)
    return result


def main():
    return sum_multiples_of_3_and_5_below(1000)


if __name__ == '__main__':
    print(main())
