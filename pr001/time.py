#!/usr/bin/python3

import timeit


def main():
    problem = 1
    setup = "from pr{:03d} import program".format(problem)
    number = 1000
    time = timeit.timeit("program.main()", setup=setup, number=number)
    print("{:f} s".format(time/number))


if __name__ == '__main__':
    main()
