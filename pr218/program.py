#!/usr/bin/python3
def main():
    # Let u and v be integers.
    # u²-v² 2uv u²+v² are the sides of the triangle.
    # u²+v² is also a square so there exist a and b such that u=a²-b² v=2ab
    # u²-v²=(a²-b²)²-4a²b²=a^4+b^4-8a²b²
    # 2uv=4ab(a²-b²)
    # A=(u²-v²)*uv/2=(a²-b²-2ab)(a²-b²+2ab)4ab(a-b)(a+b)/2 multiple of 3,4 and7
    return 0


if __name__ == '__main__':
    print(main())
