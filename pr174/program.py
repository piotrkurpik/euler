#!/usr/bin/python3
def tiles_for_which_it_can_be_form_n_to_m_ways(n, m, limit=10**6):
    tab = [0]*(limit+1)
    for i in range(3, limit//4+2):
        for j in range(i-2, 0, -2):
            if i*i - j*j > limit:
                break
            tab[i*i - j*j] += 1
    return sum([1 for i in tab if i >= n and i <= m])


def main():
    return tiles_for_which_it_can_be_form_n_to_m_ways(1, 10)


if __name__ == '__main__':
    print(main())
