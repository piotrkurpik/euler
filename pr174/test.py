#!/usr/bin/python3

import unittest
from os import path
from pr174 import program


class Problem174TestCase(unittest.TestCase):
    answer_file = path.join(path.dirname(path.realpath(__file__)), 'answer')

    def test_15_ways(self):
        result = program.tiles_for_which_it_can_be_form_n_to_m_ways(15, 15)
        expected = 832
        self.assertEqual(result, expected)

    @unittest.skipUnless(path.isfile(answer_file), "no answer file exists")
    def test_main(self):
        result = str(program.main())
        with open(self.answer_file) as a:
            expected = a.read().split("\n")[0]
        self.assertEqual(result, expected)


if __name__ == '__main__':
    unittest.main(verbosity=2)
