#!/usr/bin/python3

import unittest
from os import path
from pr051 import program


class Problem051TestCase(unittest.TestCase):
    answer_file = path.join(path.dirname(path.realpath(__file__)), 'answer')

    def test_x3(self):
        result = program.primes_with_replacement('*3')
        expected = 6
        self.assertEqual(len(result), expected)

    def test_56xx3(self):
        result = program.primes_with_replacement('56**3')
        expected = 7
        self.assertEqual(len(result), expected)

    @unittest.skipUnless(path.isfile(answer_file), "no answer file exists")
    def test_main(self):
        result = str(program.main())
        with open(self.answer_file) as a:
            expected = a.read().split("\n")[0]
        self.assertEqual(result, expected)


if __name__ == '__main__':
    unittest.main(verbosity=2)
