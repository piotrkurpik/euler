#!/usr/bin/python3
from eulerlib.numbers import is_prime
from itertools import count


def primes_with_replacement(number):
    digits = list(range(10))
    if number[0] == '*':
        digits.remove(0)
    possible_primes = [int(number.replace('*', str(i))) for i in digits]
    return [p for p in possible_primes if is_prime(p)]


def main():
    # replacing part must be of length divisible by 3
    # other way at least one of eight would be divisible by 3
    # thus it must be at least 4 digits length
    # three replacable and last digit (can't be even or 5)
    for i in count(1001, 2):
        if is_prime(i):
            s = str(i)
            for d in set(s):
                if s.count(d) % 3 == 0:
                    if len(primes_with_replacement(s.replace(d, '*'))) == 8:
                        return i


if __name__ == '__main__':
    print(main())
