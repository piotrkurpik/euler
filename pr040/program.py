#!/usr/bin/python3
def nth_digit(n):
    n, d = n-1, 1
    while True:
        digits = d*(10**d-10**(d-1))
        if n < digits:
            number = 10**(d-1) + n//d
            sign = n % d
            return int(str(number)[sign])
        n, d = n-digits, d+1
    return d


def main():
    product = 1
    for i in range(7):
        product *= nth_digit(10**i)
    return product


if __name__ == '__main__':
    print(main())
