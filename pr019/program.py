#!/usr/bin/python3
def days_in_month(month=1, year=1900):
    if month in [1, 3, 5, 7, 8, 10, 12]:
        return 31
    if month in [4, 6, 9, 11]:
        return 30
    if month == 2:
        if year % 4 == 0 and year % 100 != 0 or year % 400 == 0:
            return 29
        return 28


def main():
    day = 1  # 1 Jan 1900 was a Monday.
    for m in range(1, 13):
        day = (day + days_in_month(m, 1900)) % 7
    sundays = 0
    for y in range(1901, 2001):
        for m in range(1, 13):
            if day == 0:  # 0 means Sunday
                sundays += 1
            day = (day + days_in_month(m, y)) % 7
    return sundays


if __name__ == '__main__':
    print(main())
