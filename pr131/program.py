#!/usr/bin/python3
from itertools import count
from eulerlib.numbers import is_prime


def numbers_below(n):
    numbers = []
    for i in count():
        p = 3*i*(i+1)+1
        if p > n:
            return len(numbers)
        if is_prime(p):
            numbers.append(p)


def main():
    return numbers_below(10**6)


if __name__ == '__main__':
    print(main())
