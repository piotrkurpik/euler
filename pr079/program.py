#!/usr/bin/python3
from os import path
from itertools import permutations


def read_data():
    filename = "keylog.txt"
    with open(path.join(path.dirname(path.realpath(__file__)), filename)) as f:
        lines = [line.rstrip() for line in f.readlines()]
    pairs = set()
    for line in lines:
        pairs.add((line[0], line[1]))
        pairs.add((line[1], line[2]))
    return pairs


def is_valid(perm, pairs):
    p = list(perm)
    for pair in pairs:
        if p.index(pair[0]) > p.index(pair[1]):
            return False
    return True


def main():
    pairs = read_data()
    numbers = set()
    for pair in pairs:
        numbers.add(pair[0])
        numbers.add(pair[1])
    for perm in permutations(numbers):
        if is_valid(perm, pairs):
            return int("".join(perm))


if __name__ == '__main__':
    print(main())
