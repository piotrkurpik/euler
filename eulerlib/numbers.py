#!/usr/bin/python3
import math


def binomial_coefficient(n, k):
    if k > n - k:
        k = n - k
    if k == 0:
        return 1
    c = 1
    for i in range(k):
        c = c * (n - i) // (i + 1)
    return c


def fibonnaci_below(n):
    a, b = 1, 1
    while a < n:
        yield a
        a, b = b, a+b


def gcd(a, b):
    if b > a:
        a, b = b, a
    return ordered_gcd(a, b)


def ordered_gcd(greater, smaller):
    while True:
        if smaller == 0:
            return greater
        greater, smaller = smaller, greater % smaller


def sieve_of_eratosthenes(n):
    numbers = [False, False, True, True] + [False, True] * int((n+1)/2)
    numbers = numbers[:n+1]
    m = 3
    while m * m < n:
        if numbers[m]:
            for e in range(m * m, n + 1,  2*m):
                numbers[e] = False
        m += 1
    return [i for i in range(n) if numbers[i]]


def first_n_primes(n):
    limit = int(n*math.log(n)*1.2)  # multiplied approximations of nth prime
    limit = max(limit, 100)  # get minimum 100 primes
    primes = sieve_of_eratosthenes(limit)
    if len(primes) < n:
        return [0]
    return primes[:n]


def sum_of_divisors(n):
    divisors_sum = 1
    s = math.sqrt(n)
    for i in range(2, int(s)+1):
        if not n % i:
            divisors_sum += i + n//i
    if s == int(s):
        divisors_sum -= int(s)
    return divisors_sum


def is_prime(n):
    if n == 2:
        return True
    if n < 2 or not n % 2:
        return False
    for i in range(3, int(math.sqrt(n))+1, 2):
        if not n % i:
            return False
    return True


def is_palindromic(number):
    s = str(number)
    return s == s[::-1]


def is_palindromic_base2(number):
    b = str(bin(number)).replace('0b', '')
    return b == b[::-1]


def sum_of_digits(n):
    return sum([int(i) for i in str(n)])
