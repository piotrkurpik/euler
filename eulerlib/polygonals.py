from math import sqrt


def polygonal(p, n):
    return n*((p-2)*n+4-p)//2


def triangular_number(n):
    return polygonal(3, n)


def square(n):
    return polygonal(4, n)


def pentagonal(n):
    return polygonal(5, n)


def hexagonal(n):
    return polygonal(6, n)


def heptagonal(n):
    return polygonal(7, n)


def octagonal(n):
    return polygonal(8, n)


def is_triangular(n):
    s = int(sqrt(n*2))
    return n == s*(s+1)//2


def is_pentagonal(n):
    s = int(sqrt(2*n//3))+1
    return s*(3*s-1)//2 == n


def pyramid_number(n):
    return n*(n+1)*(2*n+1)//6
