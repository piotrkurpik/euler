def find_pythagorean_where_sum(n):
    for a in range(1, n//3):
        #  a^2 + b^2 = (n-a-b)^2
        #  =>
        #  b = n*(n/2-a)/(n-a)
        if n*(n/2-a) % (n-a) == 0:
            b = int(n*(n/2-a)//(n-a))
            if a < b:
                yield (a, b, n-a-b)
