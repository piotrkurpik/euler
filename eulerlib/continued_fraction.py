from math import sqrt


def sqrt_continued_fraction(number):
    s = sqrt(number)
    integer = int(s)
    n = s + integer
    d = number - integer*integer
    a = d
    b = int(n//d)*a - integer
    denominators = [int(n//d)]
    while (a, b) != (1, int(s)):
        n = a*(s + b)
        d = number - b*b
        k = int(n//d)
        a = int(d//a)
        b = int(k*a - b)
        denominators.append(k)
    return integer, denominators


def approximations(integer, denominators):
    prev = (1, 0)
    last = (integer, 1)
    yield last
    for i in denominators:
        new_n = prev[0] + i*last[0]
        new_d = prev[1] + i*last[1]
        prev = last
        last = (new_n, new_d)
        yield last


def sqrt_approximations(number):
    integer, cycle = sqrt_continued_fraction(number)

    def infinity_loop():
        while True:
            yield from cycle

    for x, y in approximations(integer, infinity_loop()):
        yield x, y
