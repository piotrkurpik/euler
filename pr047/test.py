#!/usr/bin/python3

import unittest
from os import path
from pr047 import program


class Problem047TestCase(unittest.TestCase):
    answer_file = path.join(path.dirname(path.realpath(__file__)), 'answer')

    def test_2_primes_factors(self):
        result = program.n_consecutive_numbers_with_n_factors(2)
        expected = 14
        self.assertEqual(result, expected)

    def test_3_primes_factors(self):
        result = program.n_consecutive_numbers_with_n_factors(3)
        expected = 644
        self.assertEqual(result, expected)

    @unittest.skipUnless(path.isfile(answer_file), "no answer file exists")
    def test_main(self):
        result = str(program.main())
        with open(self.answer_file) as a:
            expected = a.read().split("\n")[0]
        self.assertEqual(result, expected)


if __name__ == '__main__':
    unittest.main(verbosity=2)
