#!/usr/bin/python3
from math import sqrt
from itertools import count


def distinct_prime_factors_equals(n, k):
    factors = set()
    while n % 2 == 0:
        factors.add(2)
        n //= 2
    i = 3
    while n > 1:
        if i > sqrt(n):
            return len(factors) == k - 1
        while n % i == 0:
            factors.add(i)
            n //= i
            if len(factors) == k:
                return n == 1
        i += 2
    return False


def n_consecutive_numbers_with_n_factors(n):
    numbers = 0
    for i in count(4):
        if distinct_prime_factors_equals(i, n):
            numbers += 1
            if numbers == n:
                return i - n + 1
        else:
            numbers = 0


def main():
    return n_consecutive_numbers_with_n_factors(4)


if __name__ == '__main__':
    print(main())
