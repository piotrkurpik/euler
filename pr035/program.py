#!/usr/bin/python3
from eulerlib.numbers import sieve_of_eratosthenes


def circulars(n):
    for i in range(len(n)):
        yield (n+n)[i:i+len(n)]


def count_circular_primes_below(n):
    primes = [str(p) for p in sieve_of_eratosthenes(n) if not (
              '0' in str(p) or
              '2' in str(p) or
              '4' in str(p) or
              '6' in str(p) or
              '8' in str(p) or
              '5' in str(p))]
    count = 2  # 2 and 5
    for prime in primes:
        valid = 1
        for c in circulars(prime):
            if c not in primes:
                valid = 0
                break
        count += valid
    return count


def main():
    return count_circular_primes_below(10**6)


if __name__ == '__main__':
    print(main())
