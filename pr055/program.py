#!/usr/bin/python3
from eulerlib.numbers import is_palindromic


def sum_with_reverse(i):
    return i + int(str(i)[::-1])


def is_lychrel(number):
    iter = 0
    while iter < 50:
        number = sum_with_reverse(number)
        if is_palindromic(number):
            return False
        iter += 1
    return True


def main():
    return len([i for i in range(10000) if is_lychrel(i)])


if __name__ == '__main__':
    print(main())
