#!/usr/bin/python3
from eulerlib.geometry import find_pythagorean_where_sum


def main():
    triplet = next(find_pythagorean_where_sum(1000))
    return triplet[0]*triplet[1]*triplet[2]


if __name__ == '__main__':
    print(main())
