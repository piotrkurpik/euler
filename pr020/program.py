#!/usr/bin/python3
from math import factorial
from eulerlib.numbers import sum_of_digits


def sum_of_digits_in_factorial(n):
    return sum_of_digits(factorial(n))


def main():
    return sum_of_digits_in_factorial(100)


if __name__ == '__main__':
    print(main())
