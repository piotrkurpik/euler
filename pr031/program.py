#!/usr/bin/python3
def main():
    ways = 0
    for a in range(0, 201, 200):
        for b in range(0, 201-a, 100):
            for c in range(0, 201-a-b, 50):
                for d in range(0, 201-a-b-c, 20):
                    for e in range(0, 201-a-b-c-d, 10):
                        for f in range(0, 201-a-b-c-d-e, 5):
                            ways += (200-a-b-c-d-e-f)//2 + 1
    return ways


if __name__ == '__main__':
    print(main())
