#!/usr/bin/python3
from math import sqrt


def rectangles_in_grid(x, y):
    return x*(x+1)*y*(y+1)//4


def find_grid_closest(n):
    closest, diff = 0, n
    limit = int(sqrt(4*n))
    for x in range(1, limit):
        y0 = int(sqrt(4*n//(x*x+x)))
        for y in [y0, y0+1]:
            r = rectangles_in_grid(x, y)
            if abs(r - n) < diff:
                diff = abs(r - n)
                closest = x*y
    return closest


def main():
    return find_grid_closest(2000000)


if __name__ == '__main__':
    print(main())
