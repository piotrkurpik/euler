#!/usr/bin/python3
from decimal import getcontext, Decimal


def sum_of_root_digits(n, digits=100):
    getcontext().prec = digits+2
    return sum([int(s) for s in str(Decimal(n).sqrt()).replace('.', '')[:100]])


def is_square(i):
    return int(i**0.5) == i**0.5


def main():
    return sum([sum_of_root_digits(i) for i in range(100) if not is_square(i)])


if __name__ == '__main__':
    print(main())
