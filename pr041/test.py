#!/usr/bin/python3

import unittest
from os import path
from pr041 import program


class Problem041TestCase(unittest.TestCase):
    answer_file = path.join(path.dirname(path.realpath(__file__)), 'answer')

    def test_2143_is_pandigital_prime(self):
        primes = program.find_pandigital_prime()
        self.assertTrue(2143 in list(primes))

    @unittest.skipUnless(path.isfile(answer_file), "no answer file exists")
    def test_main(self):
        result = str(program.main())
        with open(self.answer_file) as a:
            expected = a.read().split("\n")[0]
        self.assertEqual(result, expected)


if __name__ == '__main__':
    unittest.main(verbosity=2)
