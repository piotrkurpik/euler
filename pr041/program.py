#!/usr/bin/python3
from itertools import permutations
from eulerlib.numbers import is_prime


def find_pandigital_prime():
    # impossible for 8 and 9 digits pandigitals to be primes (sum % 3 == 0)
    # the same for 6, 5, 3, 2 and obviously 1
    for i in [7, 4]:
        for p in permutations(range(i, 0, -1)):
            n = int("".join(map(str, p)))
            if is_prime(n):
                yield n


def main():
    return next(find_pandigital_prime())


if __name__ == '__main__':
    print(main())
