#!/usr/bin/python3
from eulerlib.numbers import is_palindromic


def greatest_palindromic_product_of_n_digit_numbers(n):
    lower_limit = 10**(n-1)
    upper_limit = 10**n
    greatest = 0
    for a in range(upper_limit, lower_limit, -1):
        for b in range(upper_limit, greatest//a, -1):
            product = a * b
            if is_palindromic(product):
                greatest = max(product, greatest)
    return greatest


def main():
    return greatest_palindromic_product_of_n_digit_numbers(3)


if __name__ == '__main__':
    print(main())
