#!/usr/bin/python3
from eulerlib.numbers import first_n_primes


def nth_prime(n):
    return first_n_primes(n)[n-1]


def main():
    return nth_prime(10001)


if __name__ == '__main__':
    print(main())
