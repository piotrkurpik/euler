#!/usr/bin/python3
def pascal_triangle(n):
    row_number = 0
    row = [1]
    yield row
    while row_number < n:
        new_row = [1] + [row[i] + row[i+1] for i in range(row_number)] + [1]
        row = new_row[:]
        yield row
        row_number += 1


def main():
    count = 0
    for row in pascal_triangle(100):
        count += len([i for i in row if i > 10**6])
    return count


if __name__ == '__main__':
    print(main())
