#!/usr/bin/python3
from eulerlib.numbers import sieve_of_eratosthenes


def prime_factors(n, primes):
    i = 0
    factors = set()
    while i < len(primes) and primes[i] <= n**0.5:
        while n % primes[i] == 0:
            factors.add(primes[i])
            n //= primes[i]
        i += 1
    factors.add(n)
    factors -= {1}
    return factors


def totient(n, primes):
    nom, den = 1, 1
    for p in prime_factors(n, primes):
        nom *= p-1
        den *= p
    return n*nom//den


def count_elements(limit):
    primes = sieve_of_eratosthenes(int(limit**0.5)+1)
    return sum([totient(i, primes) for i in range(2, limit+1)])


def main():
    return count_elements(1000000)


if __name__ == '__main__':
    print(main())
