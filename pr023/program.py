#!/usr/bin/python3
from eulerlib.numbers import sum_of_divisors


def sum_of_abundant():
    limit = 28124
    is_abundant = [False] * limit
    for i in range(12, limit):
        if i < sum_of_divisors(i):
            is_abundant[i] = True
    abundant = [i for i in range(limit) if is_abundant[i]]
    sum_of_abundant = [False] * limit
    for i, first in enumerate(abundant):
        j = i
        s = abundant[j] + first
        while s < limit:
            sum_of_abundant[s] = True
            j += 1
            s = abundant[j] + first
    return sum(i for i in range(limit) if not sum_of_abundant[i])


def main():
    return sum_of_abundant()


if __name__ == '__main__':
    print(main())
