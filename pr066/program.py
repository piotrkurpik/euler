#!/usr/bin/python3
from math import sqrt
from eulerlib.continued_fraction import sqrt_approximations


def infinity_loop(loop):
    while True:
        yield from loop


def diophantine_minimal(d):
    for x, y in sqrt_approximations(d):
        if x*x - d*y*y == 1:
            return x


def d_below_n_with_greatest_x(n):
    i, greatest = 0, 0
    for d in range(1, n+1):
        if not sqrt(d) == int(sqrt(d)):
            x = diophantine_minimal(d)
            if x > greatest:
                i, greatest = d, x
    return i


def main():
    return d_below_n_with_greatest_x(1000)


if __name__ == '__main__':
    print(main())
