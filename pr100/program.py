#!/usr/bin/python3
from eulerlib.continued_fraction import sqrt_approximations


def propabilities_equals_half():
    # lets mark blue - red as d
    # then 1/2 = (r+d)(r+d-1)/(2r+d)(2r+d-1)
    # after some transformations:
    # (2d-1)^2 - 8r^2 = 1
    for dd, r in sqrt_approximations(8):
        if dd % 2:
            d = (dd+1)//2
            b = r + d
            yield(b, r)


def main():
    s = propabilities_equals_half()
    for b, r in s:
        if b + r > 10**12:
            return b


if __name__ == '__main__':
    print(main())
