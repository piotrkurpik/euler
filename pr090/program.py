#!/usr/bin/python3
from itertools import combinations


def sets_valid_for_pair(sets, pair):
    if pair[0] in sets[0] and pair[1] in sets[1]:
        return True
    if pair[1] in sets[0] and pair[0] in sets[1]:
        return True
    return False


def sets_valid_for_squares(sets):
    squares = [
        (0, 1), (0, 4), (0, 9),
        (1, 6), (2, 5), (3, 6),
        (4, 9), (6, 4), (8, 1)
    ]
    for square in squares:
        if not sets_valid_for_pair(sets, square):
            return False
    return True


def main():
    sets = [set(s) for s in combinations(range(10), 6)]
    for s in sets:
        if 6 in s or 9 in s:
            s.add(6)
            s.add(9)
    return sum([1 for p in combinations(sets, 2) if sets_valid_for_squares(p)])


if __name__ == '__main__':
    print(main())
