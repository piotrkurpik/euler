#!/usr/bin/python3
from eulerlib.numbers import sieve_of_eratosthenes


def sum_of_primes(n):
    return sum(sieve_of_eratosthenes(n))


def main():
    number = 2000000
    return sum_of_primes(number)


if __name__ == '__main__':
    print(main())
