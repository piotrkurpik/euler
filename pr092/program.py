#!/usr/bin/python3
def sum_of_digits_squares(n):
    squares = {
        '0': 0, '1': 1, '2': 4, '3': 9, '4': 16,
        '5': 25, '6': 36, '7': 49, '8': 64, '9': 81}
    return sum([squares[i] for i in str(n)])


def main():
    arrives_to_89 = set({89})
    arrives_to_1 = set({1})
    for n in range(1, 10**7):
        i = int("".join(sorted(str(n))))
        while True:
            if i in arrives_to_89:
                arrives_to_89.add(n)
                break
            if i in arrives_to_1:
                arrives_to_1.add(n)
                break
            i = sum_of_digits_squares(i)
    return len(arrives_to_89)


if __name__ == '__main__':
    print(main())
