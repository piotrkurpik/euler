#!/usr/bin/python3
from eulerlib.geometry import find_pythagorean_where_sum


def number_of_triangle_with_perimeter(p):
    return len(list(find_pythagorean_where_sum(p)))


def main():
    greatest, number = 0, 0
    for p in range(4, 1001, 2):
        n = number_of_triangle_with_perimeter(p)
        if n > greatest:
            greatest, number = n, p
    return number


if __name__ == '__main__':
    print(main())
