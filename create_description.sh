#!/bin/bash

if [ -z "${EULER_HOME}" ]; then
    EULER_HOME="${HOME}/euler"
fi

seed_file="${EULER_HOME}/template/description.txt"
problem="${1}"

usage(){
    local exitcode="${1:-0}"
    cat <<EOF
usage ./create_description.sh prXXX
     
    it parses description.txt and create description file in proper directory.

    prXXX - required, existing directory in \${EULER_HOME}
EOF
    exit "${exitcode}"
}

error(){
    [ -z "${1}" ] || echo "$1" >&2
    usage 1
}

if [ -z "${problem}" ]; then
    error "no argument"
fi
[[ -d "${EULER_HOME}/${problem}" ]] || error "no such dir ${EULER_HOME}/${problem}"

problem_number="$(sed 's/^pr0*//' <<< ${problem})"

awk "BEGIN{toggle=0};/^Problem ${problem_number}$/{toggle=1};/Answer:/{toggle=0};toggle==1" "${seed_file}" | head -n -2  | tail -n +5 | sed -e 's/^   /# /' > "${EULER_HOME}/${problem}/description.txt"
