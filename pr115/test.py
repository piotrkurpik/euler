#!/usr/bin/python3

import unittest
from os import path
from pr115 import program


class Problem115TestCase(unittest.TestCase):
    answer_file = path.join(path.dirname(path.realpath(__file__)), 'answer')

    def test_3_unit_blocks(self):
        result = program.first_len_with_ways_exceeded(3, 10**6)
        expected = 30
        self.assertEqual(result, expected)

    def test_10_unit_blocks(self):
        result = program.first_len_with_ways_exceeded(10, 10**6)
        expected = 57
        self.assertEqual(result, expected)

    @unittest.skipUnless(path.isfile(answer_file), "no answer file exists")
    def test_main(self):
        result = str(program.main())
        with open(self.answer_file) as a:
            expected = a.read().split("\n")[0]
        self.assertEqual(result, expected)


if __name__ == '__main__':
    unittest.main(verbosity=2)
