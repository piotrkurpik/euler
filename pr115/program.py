#!/usr/bin/python3
from itertools import count


def ways_to_fill_row(n=3):
    # each tuple is ways ending with black and red block
    ways = [(1, 0)] * n
    yield from [sum(w) for w in ways]
    for i in count(n):
        w_black = sum(ways[i-1])
        w_red = ways[i-1][1] + ways[i-n][0]
        ways.append((w_black, w_red))
        yield sum(ways[i])


def first_len_with_ways_exceeded(n, limit):
    for i, w in enumerate(ways_to_fill_row(n)):
        if w > limit:
            return i


def main():
    return first_len_with_ways_exceeded(50, 10**6)


if __name__ == '__main__':
    print(main())
