#!/usr/bin/python3
from eulerlib.words import map_letters_to_numbers
from eulerlib.polygonals import is_triangular
from os import path


def load_words(filename):
    with open(path.join(path.dirname(path.realpath(__file__)), filename)) as f:
        words = f.read().replace('"', '').split(",")
    return words


def is_triangle_word(word):
    return is_triangular(sum(map_letters_to_numbers(word)))


def main():
    words = load_words("words.txt")
    return len([word for word in words if is_triangle_word(word)])


if __name__ == '__main__':
    print(main())
