#!/usr/bin/python3
from itertools import permutations
from eulerlib.numbers import gcd


def main():
    # (10x+y)/(10z+x) = y/z => y = 10xz/(9z+x)
    numerator, denominator = 1, 1
    for x, z in permutations(range(1, 10), 2):
        if not 10*x*z % (9*z + x):
            y = 10*x*z//(9*z + x)
            numerator *= min(y, z)
            denominator *= max(y, z)
    return denominator//gcd(numerator, denominator)


if __name__ == '__main__':
    print(main())
