#!/usr/bin/python3

import unittest
from os import path
from pr052 import program


class Problem052TestCase(unittest.TestCase):
    answer_file = path.join(path.dirname(path.realpath(__file__)), 'answer')

    def test_125874(self):
        result = program.n_multiplies_are_permutations(125874, 2)
        self.assertTrue(result)

    @unittest.skipUnless(path.isfile(answer_file), "no answer file exists")
    def test_main(self):
        result = str(program.main())
        with open(self.answer_file) as a:
            expected = a.read().split("\n")[0]
        self.assertEqual(result, expected)


if __name__ == '__main__':
    unittest.main(verbosity=2)
