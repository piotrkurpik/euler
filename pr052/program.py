#!/usr/bin/python3
from itertools import count


def is_permutation(i, j):
    return sorted(str(i)) == sorted(str(j))


def n_multiplies_are_permutations(number, n):
    for i in range(2, n+1):
        if not is_permutation(number, i*number):
            return False
    return True


def main():
    for i in count(1):
        if n_multiplies_are_permutations(i, 6):
            return i


if __name__ == '__main__':
    print(main())
