#!/usr/bin/python3
from eulerlib.continued_fraction import sqrt_approximations


def has_same_number_of_digits(fraction):
    n, d = fraction
    return len(str(n)) == len(str(d))


def main():
    count = 0
    for i, fraction in enumerate(sqrt_approximations(2)):
        if not has_same_number_of_digits(fraction):
            count += 1
        if i == 1000:
            return count


if __name__ == '__main__':
    print(main())
