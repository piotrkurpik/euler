#!/usr/bin/python3
def sequence(n):
    index = 1
    while n > 1:
        if n % 2:
            n = 3*n + 1
        else:
            n //= 2
        index += 1
    return index


def longest_sequence_below(n):
    longest = 0
    number = 0
    for i in range(n//2, n):
        if not (i % 3 == 2 or i % 6 == 4):  # those aren't the longest for sure
            s = sequence(i)
            if s > longest:
                longest, number = s, i
    return number


def main():
    return longest_sequence_below(1000000)


if __name__ == '__main__':
    print(main())
