#!/usr/bin/python3
from itertools import combinations_with_replacement as combinations


def main():
    single_throws = list(range(1, 21)) + [25]
    double_throws = [2*i for i in single_throws]
    triple_throws = [3*i for i in single_throws[:20]]
    all_throws = single_throws + double_throws + triple_throws

    checkout_in_1_throw = double_throws
    checkout_in_2_throws = [
        a + b for a in all_throws for b in double_throws
    ]
    checkout_in_3_throws = [
        sum(a) + b for a in combinations(all_throws, 2) for b in double_throws
    ]
    all_checkouts = checkout_in_1_throw + \
        checkout_in_2_throws + \
        checkout_in_3_throws

    return len([i for i in all_checkouts if i < 100])


if __name__ == '__main__':
    print(main())
