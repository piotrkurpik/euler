#!/usr/bin/python3
from eulerlib.numbers import is_prime
from itertools import combinations, product


def generate_n_digit_with_k_of_m(n, k, m):
    base_number = str(m)*n
    for places in combinations(range(n), n-k):
        number = list(base_number)
        possible_digits = list(range(10))
        possible_digits.remove(m)
        for com in product(possible_digits, repeat=n-k):
            for i, place in enumerate(places):
                number[place] = str(com[i])
            integer = int("".join(number))
            if integer > 10**(n-1):
                yield integer


def primes_in_list(numbers):
    primes = []
    for number in numbers:
        if is_prime(number):
            primes.append(number)
    return primes


def get_max_k_with_primes(n, m):
    for i in range(n, 0, -1):
        primes = primes_in_list(generate_n_digit_with_k_of_m(n, i, m))
        if sum(primes) > 0:
            return (i, primes)


def find_solution_for_n_digits(n):
    sum_of_s = 0
    for d in range(10):
        M, primes = get_max_k_with_primes(n, d)
        S = sum(primes)
        sum_of_s += S
    return sum_of_s


def main():
    return find_solution_for_n_digits(9)


if __name__ == '__main__':
    print(main())
