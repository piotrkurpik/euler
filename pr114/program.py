#!/usr/bin/python3
def ways_to_fill_row(n, k=3):
    # each tuple is ways ending with black and red block
    ways = [(1, 0)] * k
    for i in range(k, n+1):
        w_black = sum(ways[i-1])
        w_red = ways[i-1][1] + ways[i-k][0]
        ways.append((w_black, w_red))
    return sum(ways[n])


def main():
    return ways_to_fill_row(50)


if __name__ == '__main__':
    print(main())
