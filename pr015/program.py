#!/usr/bin/python3
from eulerlib.numbers import binomial_coefficient


def number_of_routes(n):
    # we need to do 2*n moves - n right and n down.
    # thus the question is how we put n right moves in 2*n places
    # The answer is binomial coefficient
    return binomial_coefficient(2*n, n)


def main():
    return number_of_routes(20)


if __name__ == '__main__':
    print(main())
