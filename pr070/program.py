#!/usr/bin/python3
from eulerlib.numbers import sieve_of_eratosthenes


def main():
    # answer should be product of two possible big primes
    limit = 10**7
    primes = sieve_of_eratosthenes(int(1.2*limit**0.5))[::-1]
    minimum, answer = 2, 0
    for p in primes:
        for q in [k for k in primes if p*k < limit]:
            i = p*q
            totient = (p-1)*(q-1)
            if sorted(str(i)) == sorted(str(totient)):
                if i/totient < minimum:
                    minimum, answer = i/totient, i
    return answer


if __name__ == '__main__':
    print(main())
