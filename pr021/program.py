#!/usr/bin/python3
from eulerlib.numbers import sum_of_divisors


def find_amicable_below(n):
    for i in range(2, n):
        j = sum_of_divisors(i)
        if j > i:
            if sum_of_divisors(j) == i:
                yield (i, j)


def main():
    return sum(sum(i) for i in find_amicable_below(10000))


if __name__ == '__main__':
    print(main())
