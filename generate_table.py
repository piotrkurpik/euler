#!/usr/bin/python3

from os import listdir, environ
from math import sqrt


def problem_exists(n):
    return n in problems()


def latest_problem():
    return problems()[-1]


def table_sizes(latest):
    rows = int(sqrt(latest))+1
    columns = latest//rows+1
    return (rows, columns)


def problems():
    problems = [
        int(f.replace('pr', ''))
        for f in listdir(environ["EULER_HOME"]) if f.startswith("pr")
    ]
    return sorted(problems)


def red_td(text):
    return '<td style="background-color:red;color:white;">' + \
        str(text) + '</td>'


def green_td(text):
    return '<td style="background-color:green;color:white;">' + \
        str(text) + '</td>'


def main():
    r, c = table_sizes(latest_problem())
    print("<table>")
    for row in range(r):
        print("<tr>")
        for col in range(c):
            pr = row*c+col+1
            td = red_td(pr)
            if problem_exists(pr):
                td = green_td(pr)
            print(td)
        print("</tr>")
    print("</table>")


if __name__ == '__main__':
    main()
