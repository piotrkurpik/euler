#!/usr/bin/python3
from os import path


def smallest_positive(*args):
    numbers = [a for a in args if a > 0]
    if numbers:
        return min(numbers)
    return 0


def load_matrix(filename):
    with open(path.join(path.dirname(path.realpath(__file__)), filename)) as f:
        tmp_matrix = [line.rstrip().split(',') for line in f.readlines()]
    matrix = []
    for line in tmp_matrix:
        matrix.append([int(i) for i in line])
    return matrix


def add_top_and_left_zeros(matrix):
    new_matrix = [[0]*(len(matrix[0])+1)]
    for line in matrix:
        new_matrix.append([0] + line)
    return new_matrix


def find_two_ways_path(matrix):
    matrix = add_top_and_left_zeros(matrix)
    for i, line in enumerate(matrix[1:]):
        for j, value in enumerate(line[1:]):
            matrix[i+1][j+1] += smallest_positive(
                matrix[i+1][j],
                matrix[i][j+1]
            )
    return matrix[-1][-1]


def main():
    return find_two_ways_path(load_matrix("matrix.txt"))


if __name__ == '__main__':
    print(main())
