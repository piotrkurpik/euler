#!/usr/bin/python3
from eulerlib.numbers import sum_of_digits


def main():
    return max([sum_of_digits(a**b) for a in range(100) for b in range(100)])


if __name__ == '__main__':
    print(main())
