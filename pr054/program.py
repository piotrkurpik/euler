#!/usr/bin/python3
from os import path
from pr054.poker import Hand, Card


def compare_hands(cards):
    p1_hand = Hand([Card(c) for c in cards.split()[:5]])
    p2_hand = Hand([Card(c) for c in cards.split()[5:]])
    if p1_hand > p2_hand:
        return("Player1")
    if p1_hand < p2_hand:
        return("Player2")
    if p1_hand == p2_hand:
        return("Tie")


def main():
    filename = "poker.txt"
    with open(path.join(path.dirname(path.realpath(__file__)), filename)) as f:
        hands = [s.rstrip() for s in f]
    return len([1 for h in hands if compare_hands(h) == "Player1"])


if __name__ == '__main__':
    print(main())
