#!/usr/bin/python3

import unittest
from os import path
from pr054 import program


class Problem054TestCase(unittest.TestCase):
    answer_file = path.join(path.dirname(path.realpath(__file__)), 'answer')

    def test_examples(self):
        result = program.compare_hands("5H 5C 6S 7S KD 2C 3S 8S 8D TD")
        expected = "Player2"
        self.assertEqual(result, expected)
        result = program.compare_hands("5D 8C 9S JS AC 2C 5C 7D 8S QH")
        expected = "Player1"
        self.assertEqual(result, expected)
        result = program.compare_hands("2D 9C AS AH AC 3D 6D 7D TD QD")
        expected = "Player2"
        self.assertEqual(result, expected)
        result = program.compare_hands("4D 6S 9H QH QC 3D 6D 7H QD QS")
        expected = "Player1"
        self.assertEqual(result, expected)
        result = program.compare_hands("2H 2D 4C 4D 4S 3C 3D 3S 9S 9D")
        expected = "Player1"
        self.assertEqual(result, expected)

    @unittest.skipUnless(path.isfile(answer_file), "no answer file exists")
    def test_main(self):
        result = str(program.main())
        with open(self.answer_file) as a:
            expected = a.read().split("\n")[0]
        self.assertEqual(result, expected)


if __name__ == '__main__':
    unittest.main(verbosity=2)
