class Card():
    def __init__(self, short):
        self.short = short
        self.suit = short[1]
        self.figure = short[0]
        values = ['2', '3', '4', '5', '6', '7',
                  '8', '9', 'T', 'J', 'Q', 'K', 'A']
        self.value = values.index(self.figure)

    def __lt__(self, other):
        return self.value < other.value

    def ___le__(self, other):
        return self.value <= other.value

    def __eq__(self, other):
        return self.value == other.value

    def __ne__(self, other):
        return self.value != other.value

    def __gt__(self, other):
        return self.value > other.value

    def __ge__(self, other):
        return self.value >= other.value

    def __repr__(self):
        return self.short


class Hand():
    def __init__(self, cards):
        self.cards = sorted(cards)

    @property
    def is_flush(self):
        suits = set([c.suit for c in self.cards])
        if len(suits) == 1:
            return self.cards[0].suit
        return False

    @property
    def is_straight(self):
        values = [c.value for c in self.cards]
        for i in range(len(values)-1):
            if values[i+1] - values[i] != 1:
                return False
        return values[-1]

    @property
    def is_poker(self):
        if self.is_flush and self.is_straight:
            return self.is_straight
        return False

    @property
    def is_four(self):
        values = [c.value for c in self.cards]
        if values.count(values[2]) == 4:
            return values[2]
        return False

    @property
    def is_three(self):
        values = [c.value for c in self.cards]
        if values.count(values[2]) == 3:
            return values[2]
        return False

    @property
    def is_full(self):
        values = [c.value for c in self.cards]
        if values.count(values[0]) == 3 and values.count(values[-1]) == 2:
            return values[0]
        if values.count(values[0]) == 2 and values.count(values[-1]) == 3:
            return values[-1]
        return False

    @property
    def is_two_pairs(self):
        values = [c.value for c in self.cards]
        if values.count(values[1]) == 2 and values.count(values[-2]) == 2:
            return max(values[1], values[-2])
        return False

    @property
    def is_pair(self):
        values = [c.value for c in self.cards]
        for i in range(1, 4):
            if values.count(values[i]) == 2:
                return values[i]
        return False

    @property
    def rank(self):
        if self.is_poker:
            return (8, self.is_poker)
        if self.is_four:
            return (7, self.is_four)
        if self.is_full:
            return (6, self.is_full)
        if self.is_flush:
            return (5, 0)
        if self.is_straight:
            return (4, self.is_straight)
        if self.is_three:
            return (3, self.is_three)
        if self.is_two_pairs:
            return (2, self.is_two_pairs)
        if self.is_pair:
            return (1, self.is_pair)
        return (0, 0)

    def __lt__(self, other):
        me_list = list(self.rank) + [c.value for c in self.cards][::-1]
        other_list = list(other.rank) + [c.value for c in other.cards][::-1]
        for i, j in enumerate(me_list):
            if me_list[i] != other_list[i]:
                return me_list[i] < other_list[i]
        return False

    def __gt__(self, other):
        me_list = list(self.rank) + [c.value for c in self.cards][::-1]
        other_list = list(other.rank) + [c.value for c in other.cards][::-1]
        for i, j in enumerate(me_list):
            if me_list[i] != other_list[i]:
                return me_list[i] > other_list[i]
        return False

    def __eq__(self, other):
        return [c.value for c in self.cards] == [c.value for c in other.cards]

    def __repr__(self):
        return str(self.cards)
