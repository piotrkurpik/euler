#!/usr/bin/python3
from itertools import product, permutations, combinations


def operate(a, x, b):
    if x == "+":
        return a + b
    if x == "-":
        return a - b
    if x == "*":
        return a * b
    if x == "/":
        return a / b


def smallest_not_in_set(numbers):
    i = 1
    while i in numbers:
        i += 1
    return i


def find_all_targets(numbers):
    targets = set()
    for a, b, c, d, in permutations(numbers):
        for x, y, z in product('+-*/', repeat=3):
            try:
                n = operate(a, x, b)
                n = operate(n, y, c)
                n = operate(n, z, d)
                if int(n) == n and n > 0:
                    targets.add(int(n))
            except ZeroDivisionError:
                pass
            try:
                s = operate(a, x, b)
                t = operate(c, y, d)
                n = operate(s, z, t)
                if int(n) == n and n > 0:
                    targets.add(int(n))
            except ZeroDivisionError:
                pass
    return targets


def main():
    longest, answer = 0, []
    for numbers in combinations(range(0, 10), 4):
        targets = find_all_targets(numbers)
        if smallest_not_in_set(targets) > longest:
            longest, answer = smallest_not_in_set(targets), numbers
    return "".join([str(i) for i in answer])


if __name__ == '__main__':
    print(main())
