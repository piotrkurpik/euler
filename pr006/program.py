#!/usr/bin/python3
from eulerlib.polygonals import triangular_number, pyramid_number


def sum_of_squares_minus_square_of_sum(n):
    sum_of_squares = pyramid_number(n)
    square_of_sum = triangular_number(n)**2
    return square_of_sum - sum_of_squares


def main():
    return sum_of_squares_minus_square_of_sum(100)


if __name__ == '__main__':
    print(main())
