#!/usr/bin/python3

import timeit


def main():
    setup = "from pr006 import program"
    number = 1000
    time = timeit.timeit("program.main()", setup=setup, number=number)
    print("{:f} s".format(time/number))


if __name__ == '__main__':
    main()
