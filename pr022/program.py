#!/usr/bin/python3
from os import path
from eulerlib.words import map_letters_to_numbers


def load_names(filename):
    with open(path.join(path.dirname(path.realpath(__file__)), filename)) as f:
        names = f.read().replace('"', '').split(",")
    return sorted(names)


def main():
    names = load_names("names.txt")
    scores = 0
    for i, name in enumerate(names):
        scores += (i+1) * sum(map_letters_to_numbers(name))
    return scores


if __name__ == '__main__':
    print(main())
