#!/usr/bin/python3

import unittest
from os import path
from pr022 import program


class Problem022TestCase(unittest.TestCase):
    answer_file = path.join(path.dirname(path.realpath(__file__)), 'answer')

    def test_name_sorting(self):
        result = program.load_names('names.txt').index('COLIN')
        expected = 937
        self.assertEqual(result, expected)

    def test_COLIN_score(self):
        result = sum(program.map_letters_to_numbers('COLIN'))
        expected = 53
        self.assertEqual(result, expected)

    @unittest.skipUnless(path.isfile(answer_file), "no answer file exists")
    def test_main(self):
        result = str(program.main())
        with open(self.answer_file) as a:
            expected = a.read().split("\n")[0]
        self.assertEqual(result, expected)


if __name__ == '__main__':
    unittest.main(verbosity=2)
