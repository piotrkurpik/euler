#!/usr/bin/python3
from itertools import product
from os import path


def sum_if_text(cipher, password):
    accepted = list(range(ord('a'), ord('z')+1))
    accepted += list(range(ord('A'), ord('Z')+1))
    accepted += list(range(ord('0'), ord('9')+1))
    accepted += [ord(c) for c in " .,()'!;"]
    s = 0
    for i, c in enumerate(cipher):
        d = c ^ password[i % 3]
        s += d
        if d not in accepted:
            return False
    return s


def main():
    filename = "cipher.txt"
    with open(path.join(path.dirname(path.realpath(__file__)), filename)) as f:
        cipher = f.read().replace('"', '').rstrip().split(",")
    cipher = [int(n) for n in cipher]
    for password in product(range(ord('a'), ord('z')+1), repeat=3):
        if sum_if_text(cipher, password):
            return sum_if_text(cipher, password)


if __name__ == '__main__':
    print(main())
