#!/usr/bin/python3

import unittest
from os import path
from pr017 import program


class Problem017TestCase(unittest.TestCase):
    answer_file = path.join(path.dirname(path.realpath(__file__)), 'answer')

    def test_5_numbers(self):
        result = program.sum_letters_below(5)
        expected = 19
        self.assertEqual(result, expected)

    def test_342(self):
        result = program.number_of_letters(342)
        expected = 23
        self.assertEqual(result, expected)

    def test_115(self):
        result = program.number_of_letters(115)
        expected = 20
        self.assertEqual(result, expected)

    @unittest.skipUnless(path.isfile(answer_file), "no answer file exists")
    def test_main(self):
        result = str(program.main())
        with open(self.answer_file) as a:
            expected = a.read().split("\n")[0]
        self.assertEqual(result, expected)


if __name__ == '__main__':
    unittest.main(verbosity=2)
