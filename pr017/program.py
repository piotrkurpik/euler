#!/usr/bin/python3
def letters_in_one_digit(n):
    letters = {0: 0, 1: 3, 2: 3, 3: 5, 4: 4, 5: 4, 6: 3, 7: 5, 8: 5, 9: 4}
    return letters[n]


def letters_in_two_digits(n):
    letters = {10: 3, 11: 6, 12: 6, 13: 8, 14: 8, 15: 7, 16: 7, 17: 9, 18: 8,
               19: 8, 20: 6, 30: 6, 40: 5, 50: 5, 60: 5, 70: 7, 80: 6, 90: 6}
    if n < 10:
        return letters_in_one_digit(n)
    if n % 10 == 0 or n < 20:
        return letters[n]
    else:
        return letters[n - n % 10] + letters_in_one_digit(n % 10)


def letters_in_three_digits(n):
    hundred = len("hundred")
    if n < 100:
        return letters_in_two_digits(n)
    if n % 100 == 0:
        return letters_in_one_digit(n//100) + hundred
    else:
        a, b = n//100, n % 100
        return letters_in_one_digit(a) + hundred + 3 + letters_in_two_digits(b)


def letters_in_four_digits(n):
    thousand = len("thousand")
    if n < 1000:
        return letters_in_three_digits(n)
    if n % 1000 == 0:
        return letters_in_one_digit(n//1000) + thousand
    else:
        a, b = n//1000, n % 1000
        return letters_in_one_digit(a) + thousand + letters_in_three_digits(b)


def number_of_letters(n):
    return letters_in_four_digits(n)


def sum_letters_below(n):
    return sum([number_of_letters(i) for i in range(1, n+1)])


def main():
    return sum_letters_below(1000)


if __name__ == '__main__':
    print(main())
