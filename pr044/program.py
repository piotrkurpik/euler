#!/usr/bin/python3
from eulerlib.polygonals import pentagonal, is_pentagonal
from itertools import count


def main():
    pentagonals = []
    for i in count(1):
        p = pentagonal(i)
        pentagonals.append(p)
        for q in pentagonals[:-1]:
            if is_pentagonal(p+q):
                if is_pentagonal(p+q+q):
                    return p


if __name__ == '__main__':
    print(main())
