#!/usr/bin/python3

import unittest
from os import path
from pr049 import program


class Problem049TestCase(unittest.TestCase):
    answer_file = path.join(path.dirname(path.realpath(__file__)), 'answer')

    def test_148748178147(self):
        result = 148748178147 in program.prime_permutation_sequences()
        self.assertTrue(result)

    @unittest.skipUnless(path.isfile(answer_file), "no answer file exists")
    def test_main(self):
        result = str(program.main())
        with open(self.answer_file) as a:
            expected = a.read().split("\n")[0]
        self.assertEqual(result, expected)


if __name__ == '__main__':
    unittest.main(verbosity=2)
