#!/usr/bin/python3
from eulerlib.numbers import sieve_of_eratosthenes
from itertools import combinations


def is_permutation(i, j):
    return sorted(str(i)) == sorted(str(j))


def prime_permutation_sequences():
    primes = [p for p in sieve_of_eratosthenes(10000) if p > 1000]
    answers = []
    for i, j in combinations(primes, 2):
        if is_permutation(i, j):
            k = 2*j - i
            if k in primes and is_permutation(i, k):
                answers.append(10**8*i + 10**4*j + k)
    return answers


def main():
    permutations = prime_permutation_sequences()
    permutations.remove(148748178147)
    return permutations[0]


if __name__ == '__main__':
    print(main())
