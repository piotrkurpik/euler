#!/usr/bin/python3
from eulerlib.numbers import gcd


def triangles_within_n_grid_with_right_angle_in(x, y, n):
    count = 0
    g = gcd(x, y)
    dx, dy = -1 * y//g, x//g
    d = 1
    while x-dx*d <= n and y-d*dy >= 0:
        count += 1
        d += 1
    d = 1
    while x+dx*d >= 0 and y+d*dy <= n:
        count += 1
        d += 1
    return count


def right_triangle_within_grid(n):
    count = 3*n*n
    for i in range(1, n+1):
        for j in range(1, n+1):
            count += triangles_within_n_grid_with_right_angle_in(i, j, n)
    return count


def main():
    return right_triangle_within_grid(50)


if __name__ == '__main__':
    print(main())
