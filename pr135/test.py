#!/usr/bin/python3

import unittest
from os import path
from pr135 import program


class Problem135TestCase(unittest.TestCase):
    answer_file = path.join(path.dirname(path.realpath(__file__)), 'answer')

    def test_27(self):
        result = program.solutions(27)
        expected = 2
        self.assertEqual(result, expected)

    def test_1155(self):
        result = program.solutions(1155)
        expected = 10
        self.assertEqual(result, expected)

    @unittest.skipUnless(path.isfile(answer_file), "no answer file exists")
    def test_main(self):
        result = str(program.main())
        with open(self.answer_file) as a:
            expected = a.read().split("\n")[0]
        self.assertEqual(result, expected)


if __name__ == '__main__':
    unittest.main(verbosity=2)
