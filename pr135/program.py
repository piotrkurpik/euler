#!/usr/bin/python3
from itertools import product
from eulerlib.numbers import sieve_of_eratosthenes


primes = sieve_of_eratosthenes(1000)


def mul(ls):
    m = ls[0]
    for i in ls[1:]:
        m *= i
    return m


def factors(n):
    factors = []
    for p in primes:
        if p > n:
            return [n] + factors
        while n % p == 0:
            factors.append(p)
            n //= p
    return [n] + factors


def divisors(n):
    f = factors(n)
    f = [[i**j for j in range(0, f.count(i)+1)] for i in set(f) if i > 1]
    return [mul(k) for k in product(*f)]


def solutions(n):
    return sum([(i + n//i) % 4 == 0 for i in divisors(n) if 3*i*i > n])


def main():
    s = sum([solutions(i) == 10 for i in range(3, 1000000, 4)])
    s += sum([solutions(i) == 10 for i in range(4, 1000000, 4)])
    return s


if __name__ == '__main__':
    print(main())
