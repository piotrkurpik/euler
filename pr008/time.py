#!/usr/bin/python3

import timeit


def main():
    setup = "from pr008 import program"
    number = 100
    time = timeit.timeit("program.main()", setup=setup, number=number)
    print("{:f} s".format(time/number))


if __name__ == '__main__':
    main()
