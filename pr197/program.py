#!/usr/bin/python3
def f(x):
    return int(2**(30.403243784-x**2)) * 10**-9


def main():
    p = -1
    x = f(p)
    n = f(x)
    while p != n:
        p = x
        x = n
        n = f(n)
    return(x+n)


if __name__ == '__main__':
    print(main())
