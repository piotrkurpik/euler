#!/usr/bin/python3
from eulerlib.numbers import is_prime
from itertools import count


def first_n_exceeding_limit(limit):
    n = 0
    for i in count(2):
        if is_prime(i):
            n += 1
            r = 2
            if n % 2 == 1:
                r = (2*n*i) % (i*i)
            if r > limit:
                return n


def main():
    return first_n_exceeding_limit(10**10)


if __name__ == '__main__':
    print(main())
