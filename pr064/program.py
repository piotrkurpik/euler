#!/usr/bin/python3
from math import sqrt
from eulerlib.continued_fraction import sqrt_continued_fraction


def get_period_length(n):
    return len(sqrt_continued_fraction(n)[1])


def find_odd_periods_below(n):
    count = 0
    for i in range(n):
        if not sqrt(i) == int(sqrt(i)):
            if get_period_length(i) % 2:
                count += 1
    return count


def main():
    return find_odd_periods_below(10000)


if __name__ == '__main__':
    print(main())
