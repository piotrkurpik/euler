#!/usr/bin/python3

import unittest
from os import path
from pr064 import program


class Problem064TestCase(unittest.TestCase):
    answer_file = path.join(path.dirname(path.realpath(__file__)), 'answer')

    def test_continued_fraction_for_sqrt23(self):
        result = program.get_period_length(23)
        expected = 4
        self.assertEqual(result, expected)

    def test_square_roots_below_14(self):
        result = program.find_odd_periods_below(14)
        expected = 4
        self.assertEqual(result, expected)

    @unittest.skipUnless(path.isfile(answer_file), "no answer file exists")
    def test_main(self):
        result = str(program.main())
        with open(self.answer_file) as a:
            expected = a.read().split("\n")[0]
        self.assertEqual(result, expected)


if __name__ == '__main__':
    unittest.main(verbosity=2)
