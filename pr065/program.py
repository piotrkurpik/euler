#!/usr/bin/python3
from eulerlib.continued_fraction import approximations
from eulerlib.numbers import sum_of_digits
from itertools import count


def e_fraction_denominators():
    for i in count(2, 2):
        yield from [1, i, 1]


def e_nth_nominator_sum(n):
    for i, nom in enumerate(approximations(2, e_fraction_denominators())):
        if i + 1 == n:
            return sum_of_digits(nom[0])


def main():
    return e_nth_nominator_sum(100)


if __name__ == '__main__':
    print(main())
