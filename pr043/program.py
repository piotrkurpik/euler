#!/usr/bin/python3
from itertools import permutations


def is_awesome_pandigital(d):
    if d[0] == 0:
        return False
    if d[3] not in [0, 2, 4, 6, 8]:
        return False
    if d[5] not in [0, 5]:
        return False
    if (d[2]+d[3]+d[4]) % 3:
        return False
    if (100*d[4]+10*d[5]+d[6]) % 7:
        return False
    if (100*d[5]+10*d[6]+d[7]) % 11:
        return False
    if (100*d[6]+10*d[7]+d[8]) % 13:
        return False
    if (100*d[7]+10*d[8]+d[9]) % 17:
        return False
    return True


def main():
    s = []
    for p in permutations(range(10)):
        if is_awesome_pandigital(p):
            s.append(p)
    return sum([int("".join(map(str, i))) for i in s])


if __name__ == '__main__':
    print(main())
