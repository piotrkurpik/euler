#!/usr/bin/python3
def without_2s_and_5s(n):
    while n % 2 == 0:
        n //= 2
    while n % 5 == 0:
        n //= 5
    return n


def sum_of_Ds(n):
    s = n*(n+1)//2 - 4*5//2
    k = 1
    w = without_2s_and_5s(k)
    kk = k**k
    k1k1 = (k+1)**(k+1)
    for i in range(5, n+1):
        if k1k1 < i*kk:
            k += 1
            w = without_2s_and_5s(k)
            kk = k**k
            k1k1 = (k+1)**(k+1)
            w = without_2s_and_5s(k)
        if not i % w:
            s -= 2*i
    return s


def main():
    return sum_of_Ds(10000)


if __name__ == '__main__':
    print(main())
