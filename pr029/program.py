#!/usr/bin/python3
def powers(n):
    numbers = set()
    for i in range(2, n+1):
        for j in range(2, n+1):
            numbers.add(i**j)
    return len(numbers)


def main():
    return powers(100)


if __name__ == '__main__':
    print(main())
