#!/usr/bin/python3
from itertools import permutations


def lexicographic_permutation(numbers, which):
    i = 0
    for per in permutations(range(numbers)):
        i += 1
        if i == which:
            return "".join(map(str, per))


def main():
    return lexicographic_permutation(10, 1000000)


if __name__ == '__main__':
    print(main())
