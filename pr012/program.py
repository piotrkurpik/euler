#!/usr/bin/python3
from math import sqrt
from eulerlib.polygonals import triangular_number
from itertools import count


def divisors(n):
    divisors = 0
    for i in range(1, int(sqrt(n))+1):
        if n % i == 0:
            divisors += 2
    if int(sqrt(n)) == sqrt(n):
        divisors -= 1
    return divisors


def first_triangular_with_n_divisors(n):
    for i in count(1):
        triangular = triangular_number(i)
        if divisors(triangular) >= n:
            return triangular


def main():
    return first_triangular_with_n_divisors(500)


if __name__ == '__main__':
    print(main())
