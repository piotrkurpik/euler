#!/usr/bin/python3
from itertools import permutations


def is_concatenated_products(digits):
    for d in range(len(digits)//2):
        tmp_digits = digits[:d+1]
        first = int("".join(map(str, tmp_digits)))
        m = 2
        while len(tmp_digits) < len(digits):
            tmp_digits += list(map(int, str(first * m)))
            m += 1
        if tmp_digits == digits:
            return True
    return False


def main():
    for c in permutations(range(9, 0, -1)):
        if is_concatenated_products(list(c)):
            return int("".join(map(str, c)))


if __name__ == '__main__':
    print(main())
