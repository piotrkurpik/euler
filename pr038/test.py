#!/usr/bin/python3

import unittest
from os import path
from pr038 import program


class Problem038TestCase(unittest.TestCase):
    answer_file = path.join(path.dirname(path.realpath(__file__)), 'answer')

    def test_192384576(self):
        result = program.is_concatenated_products([1, 9, 2, 3, 8, 4, 5, 7, 6])
        self.assertTrue(result)

    @unittest.skipUnless(path.isfile(answer_file), "no answer file exists")
    def test_main(self):
        result = str(program.main())
        with open(self.answer_file) as a:
            expected = a.read().split("\n")[0]
        self.assertEqual(result, expected)


if __name__ == '__main__':
    unittest.main(verbosity=2)
