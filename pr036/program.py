#!/usr/bin/python3
from eulerlib.numbers import is_palindromic, is_palindromic_base2


def is_palindromic_both_base(n):
    if not is_palindromic(n):
        return False
    if not is_palindromic_base2(n):
        return False
    return True


def main():
    s = 0
    for i in range(1, 1000000, 2):
        if is_palindromic_both_base(i):
            s += i
    return s


if __name__ == '__main__':
    print(main())
