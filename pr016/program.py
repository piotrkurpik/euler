#!/usr/bin/python3
from eulerlib.numbers import sum_of_digits


def main():
    return sum_of_digits(2**1000)


if __name__ == '__main__':
    print(main())
