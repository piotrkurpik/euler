#!/usr/bin/python3
def equals_sum_of_digits_powers(n, p):
    return n == sum([int(i)**p for i in str(n)])


def sum_numbers_equals_sum_of_digit_powers(p):
    digits = 1
    while digits * 9**p > 10**digits - 1:
        digits += 1
    limit = 10**digits-1
    s = 0
    for i in range(10, limit):
        if equals_sum_of_digits_powers(i, p):
            s += i
    return s


def main():
    return sum_numbers_equals_sum_of_digit_powers(5)


if __name__ == '__main__':
    print(main())
