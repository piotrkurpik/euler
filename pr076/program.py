#!/usr/bin/python3
from itertools import islice, count


def ways_to_write_as_a_sum():
    ways = []
    for i in count(1):
        new_ways = [0] * i + [1]
        for j in range(1, i//2+1):
            new_ways[j] = sum(ways[0-j][j:])
        ways.append(new_ways)
        yield sum(ways[-1])-1


def ways_to_write_n_as_a_sum(n):
    return islice(ways_to_write_as_a_sum(), n-1, n).__next__()


def main():
    return ways_to_write_n_as_a_sum(100)


if __name__ == '__main__':
    print(main())
