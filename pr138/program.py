#!/usr/bin/python3
from eulerlib.continued_fraction import sqrt_approximations


def main():
    # (2a±1)^2 + a^2 = L^2
    # 5a^2 ± 4a + 1 = L^2
    # 25a^2 ± 20a + 5 = 5L^2
    # (5a±2)^2 - 5h^2 = -1
    # so let's check approximations od sqrt(5)
    s = 0
    count = 0
    for n, d in sqrt_approximations(5):
        if n**2 - 5*d**2 == -1:
            L = d
            if n % 5 == 3:
                a = (n+2)//5
            elif n % 3 == 2:
                a = (n-2)//5
            if a > 0:
                count += 1
                s += L
        if count == 12:
            return s


if __name__ == '__main__':
    print(main())
