#!/usr/bin/python3

import unittest
from os import path
from pr048 import program


class Problem048TestCase(unittest.TestCase):
    answer_file = path.join(path.dirname(path.realpath(__file__)), 'answer')

    def test_self_powers_10(self):
        result = program.self_powers(10)
        expected = 10405071317
        self.assertEqual(result, expected)

    @unittest.skipUnless(path.isfile(answer_file), "no answer file exists")
    def test_main(self):
        result = str(program.main())
        with open(self.answer_file) as a:
            expected = a.read().split("\n")[0]
        self.assertEqual(result, expected)


if __name__ == '__main__':
    unittest.main(verbosity=2)
