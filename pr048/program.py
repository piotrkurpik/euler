#!/usr/bin/python3
def self_powers(n):
    return sum([i**i for i in range(1, n+1)])


def main():
    return self_powers(1000) % 10**10


if __name__ == '__main__':
    print(main())
