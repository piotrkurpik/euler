#!/usr/bin/python3

import unittest
from os import path
from pr077 import program


class Problem077TestCase(unittest.TestCase):
    answer_file = path.join(path.dirname(path.realpath(__file__)), 'answer')

    def test_ways_to_write_10_as_sum_of_primes(self):
        result = program.ways_to_sum_with_primes(10)
        expected = 5
        self.assertEqual(result, expected)

    @unittest.skipUnless(path.isfile(answer_file), "no answer file exists")
    def test_main(self):
        result = str(program.main())
        with open(self.answer_file) as a:
            expected = a.read().split("\n")[0]
        self.assertEqual(result, expected)


if __name__ == '__main__':
    unittest.main(verbosity=2)
