#!/usr/bin/python3
from eulerlib.numbers import is_prime
from itertools import count


def prev_prime(n):
    n -= 1
    while n > 0:
        if is_prime(n):
            return n
        n -= 1
    return 0


def ways_to_sum_with_primes(n, biggest=None):
    if biggest is None:
        biggest = prev_prime(n)
    if n == 0 and (is_prime(biggest) or biggest == 0):
        return 1
    if biggest < 2:
        return 0
    if biggest == 2:
        return int(n % 2 == 0)
    if n == 1:
        return 0
    ways = 0
    i = prev_prime(biggest+1)
    while i > 1:
        for j in range(i, n+1, i):
            ways += ways_to_sum_with_primes(n-j, prev_prime(i))
        i = prev_prime(i)
    return ways


def main():
    for i in count(2):
        ways = ways_to_sum_with_primes(i)
        if ways > 5000:
            return i


if __name__ == '__main__':
    print(main())
