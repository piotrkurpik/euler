#!/usr/bin/python3
from eulerlib.numbers import is_prime
from itertools import count


def diagonal_numbers():
    n = 1
    for d in count(2, 2):
        yield [n+d, n+2*d, n+3*d, n+4*d]
        n = n+4*d


def main():
    numbers = 1
    primes = 0
    for diag in diagonal_numbers():
        numbers += 4
        primes += len([i for i in diag if is_prime(i)])
        ratio = 100*primes/numbers
        if ratio < 10:
            return diag[3] - diag[2] + 1


if __name__ == '__main__':
    print(main())
