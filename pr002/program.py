#!/usr/bin/python3
from eulerlib.numbers import fibonnaci_below


def sum_even_fibonacci_numbers_below(n):
    return sum([f for f in fibonnaci_below(n) if f % 2 == 0])


def main():
    limit = 4000000
    return sum_even_fibonacci_numbers_below(limit)


if __name__ == '__main__':
    print(main())
