#!/usr/bin/python3
from itertools import count


def divides_tribonacci(n):
    a, b, c = 1, 1, 1
    while True:
        a, b, c = b, c, (a+b+c) % n
        if (a, b, c) == (1, 1, 1):
            return False
        if c == 0:
            return True


def main():
    how_many = 0
    for i in count(27, 2):
        if not divides_tribonacci(i):
            how_many += 1
        if how_many == 124:
            return i


if __name__ == '__main__':
    print(main())
