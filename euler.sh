#!/bin/bash


if [ -z "${EULER_HOME}" ]; then
    EULER_HOME="${HOME}/euler"
fi
export PYTHONPATH=${EULER_HOME}

subcommand=${1}
problem=${2}
problem_bre="pr[0-9][0-9][0-9]"

usage(){
    local exitcode="${1:-0}"
    cat <<EOF
usage ./euler.sh command [prXXX]

where command is one of the following:
    help     - show this help and exit.
    create   - creating problem directory. Argument prXXX is required.
    run      - execute program. Argument prXXX is required.
    run_test - execute tests. Argument prXXX is optional. If not provided runs all tests.
    timeit   - calculate execution time. Argument prXXX is optional. If not provided checks all times.
    answer   - execute program and write output to answer file. Argument prXXX is required.
EOF
    exit "${exitcode}"
}

error(){
    [ -z "${1}" ] || echo "$1" >&2
    usage 1
}

create(){
    local problem="${1}"
    local problem_number
    problem_number="${problem#pr}"
    cp -r "${EULER_HOME}/template" "${EULER_HOME}/${problem}"
    for file in ${EULER_HOME}/${problem}/*; do
	sed -i -e "s/XXX/${problem_number}/g" "${file}"
    done
    "${EULER_HOME}/create_description.sh" "${problem}" > "${EULER_HOME}/${problem}/description.txt"
    touch "${EULER_HOME}/${problem}/__init__.py"
}

run(){
    local problem="${1}"
    python3 "${EULER_HOME}/${problem}/program.py"
}

run_test(){
    local problem="${1}"
    python3 "${EULER_HOME}/${problem}/test.py"
}

timeit(){
    local problem="${1}"
    python3 "${EULER_HOME}/${problem}/time.py"
}

case "${subcommand}" in
    "help")
	usage
	;;
    "create")
	[[ "${problem}" =~ ${problem_bre} ]] || error "problem should be in prXXX form"
	create "${problem}"
	;;
    "run")
	[[ "${problem}" =~ ${problem_bre} ]] || error "problem should be in prXXX form"
	[[ -d "${EULER_HOME}/${problem}" ]] || error "no such dir ${EULER_HOME}/${problem}"
	run "${problem}"
	;;
    "run_test")
	if [ -z "${problem}" ]; then
	    python3 -munittest discover -s "${EULER_HOME}" -v
	else
	    [[ "${problem}" =~ ${problem_bre} ]] || error "problem should be in prXXX form"
	    [[ -d "${EULER_HOME}/${problem}" ]] || error "no such dir ${EULER_HOME}/${problem}"
	    run_test "${problem}"
	fi
	;;
    "timeit")
	if [ -z "${problem}" ]; then
	    for dir in ${EULER_HOME}/${problem_bre}; do
		pr="$(basename "${dir}")"
		echo -e "${pr}\t$(timeit "${pr}")"
	    done
	else
	    [[ "${problem}" =~ ${problem_bre} ]] || error "problem should be in prXXX form"
	    [[ -d "${EULER_HOME}/${problem}" ]] || error "no such dir ${EULER_HOME}/${problem}"
	    timeit "${problem}"
	fi
	;;
    "answer")
	[[ "${problem}" =~ ${problem_bre} ]] || error "problem should be in prXXX form"
	[[ -d "${EULER_HOME}/${problem}" ]] || error "no such dir ${EULER_HOME}/${problem}"
	run "${problem}" > "${EULER_HOME}/${problem}/answer"
	;;
    *)
	error "no such command ${subcommand}"
	;;
esac
