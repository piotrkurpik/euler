#!/usr/bin/python3
def main():
    numbers = set(range(1, 11))
    solution = [[0, 0, 0], [0, 0, 0], [0, 0, 0], [0, 0, 0], [0, 0, 0]]
    externals = set([10])  # because it's 16-digit
    externals |= set([6, 7, 8, 9])  # divisible by 5 and greatest minimal
    internals = numbers - externals
    # sum_of_line = (sum(externals) + 2*sum(internals))//5 =  14
    solution[0][0] = 6
    externals.remove(6)
    solution[0][1], solution[0][2] = 5, 3  # greatest solution for first line
    solution[4][2], solution[1][1] = 5, 3
    internals -= set([5, 3])
    solution[2][2] = solution[3][1] = 4  # 1 and 2 can't be on the same line
    internals.remove(4)
    solution[4][0] = 7  # can't be on line with 4, because 3 is taken
    externals.remove(7)
    solution[4][1] = solution[3][2] = 2  # sum up to 14
    internals.remove(2)
    solution[3][0] = 8  # sum up to 14
    externals.remove(8)
    solution[2][1] = solution[1][2] = 1  # last internal left
    internals.remove(1)
    solution[1][0], solution[2][0] = 10, 9  # sum up to 14
    externals -= set({9, 10})
    return "".join(["".join([str(n) for n in s]) for s in solution])


if __name__ == '__main__':
    print(main())
