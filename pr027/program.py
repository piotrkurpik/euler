#!/usr/bin/python3
from eulerlib.numbers import is_prime, sieve_of_eratosthenes
from itertools import count


def find_consecutive_primes(a, b):
    for i in count(2):  # because we check only if it's prime for 0 and 1
        if not is_prime(i*i + i*a + b):
            return i


def main():
    limit = 1000
    greatest = 0
    best = (0, 0)
    primes = sieve_of_eratosthenes(2*limit)
    for b in [p for p in primes if p < limit]:  # prime for n = 0
        for a in [p-b-1 for p in primes if p-b-1 < limit]:  # prime n = 1
            n = find_consecutive_primes(a, b)
            if n > greatest:
                greatest = n
                best = (a, b)
    return best[0]*best[1]


if __name__ == '__main__':
    print(main())
