#!/usr/bin/python3

import unittest
from os import path
from pr027 import program


class Problem027TestCase(unittest.TestCase):
    answer_file = path.join(path.dirname(path.realpath(__file__)), 'answer')

    def test_consecutive_primes_for_1_41(self):
        result = program.find_consecutive_primes(1, 41)
        expected = 40
        self.assertEqual(result, expected)

    def test_consecutive_primes_for_79_1601(self):
        result = program.find_consecutive_primes(-79, 1601)
        expected = 80
        self.assertEqual(result, expected)

    @unittest.skipUnless(path.isfile(answer_file), "no answer file exists")
    def test_main(self):
        result = str(program.main())
        with open(self.answer_file) as a:
            expected = a.read().split("\n")[0]
        self.assertEqual(result, expected)


if __name__ == '__main__':
    unittest.main(verbosity=2)
