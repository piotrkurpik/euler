#!/usr/bin/python3
from eulerlib.continued_fraction import sqrt_approximations


def main():
    # (2a±1)^2 - a^2 = h^2
    # 3a^2 ± 4a + 1 = h^2
    # 9a^2 ± 12a + 3 = 3h^2
    # (3a±2)^2 - 3h^2 = 1
    # so let's check approximations od sqrt(3)
    s = 0
    for n, d in sqrt_approximations(3):
        if n**2 - 3*d**2 == 1:
            h = d
            if n % 3 == 1:
                a = (n+2)//3
            elif n % 3 == 2:
                a = (n-2)//3
            if a > 0:
                c = int((a*a + h*h)**0.5)
                s += 2*(a+c)
        if n > 10**9//3:
            return s


if __name__ == '__main__':
    print(main())
