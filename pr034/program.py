#!/usr/bin/python3
def equals_sum_of_digits_factorials(n):
    factorials = [1, 1, 2, 6, 24, 120, 720, 5040, 40320, 362880]
    return n == sum([factorials[int(i)] for i in str(n)])


def main():
    f9 = 362880
    k = 1
    while k * f9 > 10**k:
        k += 1
    n = [i for i in range(10, k*f9) if equals_sum_of_digits_factorials(i)]
    return sum(n)


if __name__ == '__main__':
    print(main())
