#!/usr/bin/python3
from pr081.program import smallest_positive, load_matrix


def add_top_and_bottom_zeros(matrix):
    new_matrix = [[0]*(len(matrix[0]))]
    for line in matrix:
        new_matrix.append(line)
    new_matrix.append([0]*(len(matrix[0])))
    return new_matrix


def find_three_ways_path(matrix):
    matrix = add_top_and_bottom_zeros(matrix)
    for i in range(1, len(matrix[0])-1):
        init_column = [line[i] for line in matrix]
        for j in range(1, len(init_column)-1):
            matrix[j][i] += matrix[j][i-1]
        last_column = init_column[:]
        column = [line[i] for line in matrix]
        while last_column != column:
            for j in range(1, len(init_column)-1):
                matrix[j][i] = init_column[j] + smallest_positive(
                    matrix[j][i-1],
                    matrix[j-1][i],
                    matrix[j+1][i]
                )
            last_column = column[:]
            column = [line[i] for line in matrix]
    last_column = [line[-2]+line[-1] for line in matrix]
    return smallest_positive(*last_column)


def main():
    return find_three_ways_path(load_matrix("matrix.txt"))


if __name__ == '__main__':
    print(main())
