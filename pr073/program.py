#!/usr/bin/python3
from eulerlib.numbers import ordered_gcd


def count_fractions_between_third_and_half(n):
    return sum(
        [len([1 for k in range(i//3+1, i//2+1) if ordered_gcd(k, i-2*k) == 1])
         for i in range(5, n+1)]
    )


def main():
    return count_fractions_between_third_and_half(12000)


if __name__ == '__main__':
    print(main())
