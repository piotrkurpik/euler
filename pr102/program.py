#!/usr/bin/python3
from os import path


def load_triangles(filename):
    with open(path.join(path.dirname(path.realpath(__file__)), filename)) as f:
        triangles = [
            [int(i) for i in line.strip().split(",")]
            for line in f.readlines()
        ]
    return triangles


def has_origin_on_the_left(section):
    dx = section[2] - section[0]
    dy = section[3] - section[1]
    try:
        a = dy/dx
    except ZeroDivisionError:
        if dy*section[2] < 0:
            return -1
        else:
            return 1
        return 0
    b = section[1] - a*section[0]
    s = a*b*dy
    if s < 0:
        return 1
    elif s > 0:
        return -1
    return 0


def contains_origin(triangle):
    s = 0
    for section in [triangle[:4], triangle[-4:], triangle[-2:] + triangle[:2]]:
        s += has_origin_on_the_left(section)
    return s*s == 9


def main():
    triangles = load_triangles("triangles.txt")
    return sum([contains_origin(t) for t in triangles])


if __name__ == '__main__':
    print(main())
