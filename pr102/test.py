#!/usr/bin/python3

import unittest
from os import path
from pr102 import program


class Problem102TestCase(unittest.TestCase):
    answer_file = path.join(path.dirname(path.realpath(__file__)), 'answer')

    def test_first_two_triangles(self):
        triangles = program.load_triangles("triangles.txt")[:2]
        self.assertTrue(program.contains_origin(triangles[0]))
        self.assertFalse(program.contains_origin(triangles[1]))

    @unittest.skipUnless(path.isfile(answer_file), "no answer file exists")
    def test_main(self):
        result = str(program.main())
        with open(self.answer_file) as a:
            expected = a.read().split("\n")[0]
        self.assertEqual(result, expected)


if __name__ == '__main__':
    unittest.main(verbosity=2)
