#!/usr/bin/python3
from eulerlib.numbers import gcd


def smallest_number_divisible_by_numbers_below(n):
    product = 1
    for i in range(1, n+1):
        product //= gcd(product, i)
        product *= i
    return product


def main():
    return smallest_number_divisible_by_numbers_below(20)


if __name__ == '__main__':
    print(main())
