#!/usr/bin/python3
def spiral_sum(n):
    return 1 + sum([4*i*i+10*(i+1) for i in range(1, n, 2)])


def main():
    return spiral_sum(1001)


if __name__ == '__main__':
    print(main())
