#!/usr/bin/python3

import unittest
from os import path
from pr018 import program


class Problem018TestCase(unittest.TestCase):
    answer_file = path.join(path.dirname(path.realpath(__file__)), 'answer')

    def test_small_triangle(self):
        triangle = """3
        7 4
        2 4 6
        8 5 9 3"""
        result = program.process_triangle(triangle)
        expected = 23
        self.assertEqual(result, expected)

    @unittest.skipUnless(path.isfile(answer_file), "no answer file exists")
    def test_main(self):
        result = str(program.main())
        with open(self.answer_file) as a:
            expected = a.read().split("\n")[0]
        self.assertEqual(result, expected)


if __name__ == '__main__':
    unittest.main(verbosity=2)
