#!/usr/bin/python3

import timeit


def main():
    setup = "from pr026 import program"
    number = 5
    time = timeit.timeit("program.main()", setup=setup, number=number)
    print("{:f} s".format(time/number))


if __name__ == '__main__':
    main()
