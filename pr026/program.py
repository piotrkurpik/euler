#!/usr/bin/python3
from itertools import count


def how_many_times_is_divisable(n, k):
    i = 0
    while not n % k:
        n, i = n//k, i+1
    return i


def find_cycle(n):
    d2 = how_many_times_is_divisable(n, 2)
    d5 = how_many_times_is_divisable(n, 5)
    if n == 2**d2 * 5**d5:
        return 0
    m = 10**max(d2, d5)
    for i in count(1):
        if (10**i - 1) * m % n == 0:
            return i


def find_greatest_cycle_below(n):
    greatest, number = 0, 0
    for i in range(2, n):
        cycle = find_cycle(i)
        if cycle > greatest:
            greatest, number = cycle, i
    return number


def main():
    return find_greatest_cycle_below(1000)


if __name__ == '__main__':
    print(main())
