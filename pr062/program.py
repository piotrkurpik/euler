#!/usr/bin/python3
from itertools import count


def find_permutations(numbers):
    permutations = {}
    for n in numbers:
        key = tuple(sorted(list(str(n))))
        if key in permutations.keys():
            permutations[key].append(n)
        else:
            permutations[key] = [n]
    return permutations.values()


def find_cubes_with_n_permutation(n):
    digits = 1
    cubes = []
    for i in count(1):
        c = i**3
        if len(str(c)) == digits:
            cubes.append(c)
        if len(str(c)) == digits + 1:
            permutations = find_permutations(cubes)
            permutations = sorted(permutations, key=lambda p: min(p))
            for p in permutations:
                if len(p) == n:
                    return p[0]
            digits += 1
            cubes = [c]


def main():
    return find_cubes_with_n_permutation(5)


if __name__ == '__main__':
    print(main())
