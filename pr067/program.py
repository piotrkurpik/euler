#!/usr/bin/python3
from pr018.program import process_triangle
from os import path


def main():
    filename = "triangle.txt"
    with open(path.join(path.dirname(path.realpath(__file__)), filename)) as f:
        data = f.read().rstrip()
    return process_triangle(data)


if __name__ == '__main__':
    print(main())
