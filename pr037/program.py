#!/usr/bin/python3
from itertools import product, count
from eulerlib.numbers import is_prime


def is_truncatable_prime(n):
    s = str(n)
    for i in range(len(s)):
        if not is_prime(int(s[:i+1])):
            return False
        if not is_prime(int(s[i:])):
            return False
    return True


def main():
    truncatable_primes = []
    first_last = [
        (2, 3), (2, 7), (3, 3), (3, 7),
        (5, 3), (5, 7), (7, 3), (7, 7)
    ]
    for digits in count():
        for middle in product([1, 3, 7, 9], repeat=digits):
            for f, l in first_last:
                number = str(f) + "".join(map(str, middle)) + str(l)
                number = int(number)
                if is_truncatable_prime(number):
                    truncatable_primes.append(number)
                    if len(truncatable_primes) == 11:
                        return sum(truncatable_primes)


if __name__ == '__main__':
    print(main())
