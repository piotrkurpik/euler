#!/usr/bin/python3
from eulerlib.numbers import sieve_of_eratosthenes


def sums_of_square_cube_and_forth_below(n):
    writable_as_sum = set()
    primes = sieve_of_eratosthenes(int(n**(1/2))+1)
    for s in primes:
        limit_c = (n-s**2)**(1/3)
        i_c = 0
        c = primes[i_c]
        while c < limit_c:
            limit_f = (n-s**2-c**3)**(1/4)
            i_f = 0
            f = primes[i_f]
            while f < limit_f:
                writable_as_sum.add(s**2+c**3+f**4)
                i_f += 1
                f = primes[i_f]
            i_c += 1
            c = primes[i_c]
    return len(writable_as_sum)


def main():
    return sums_of_square_cube_and_forth_below(50000000)


if __name__ == '__main__':
    print(main())
