#!/usr/bin/python3

import unittest
from os import path
from pr083 import program


class Problem081TestCase(unittest.TestCase):
    answer_file = path.join(path.dirname(path.realpath(__file__)), 'answer')

    def test_matrix(self):
        matrix = [
            [131, 673, 234, 103, 18],
            [201, 96, 342, 965, 150],
            [630, 803, 746, 422, 111],
            [537, 699, 497, 121, 956],
            [805, 732, 524, 37, 331]
        ]
        result = program.find_four_ways_path(matrix)
        expected = 2297
        self.assertEqual(result, expected)

    @unittest.skipUnless(path.isfile(answer_file), "no answer file exists")
    def test_main(self):
        result = str(program.main())
        with open(self.answer_file) as a:
            expected = a.read().split("\n")[0]
        self.assertEqual(result, expected)


if __name__ == '__main__':
    unittest.main(verbosity=2)
