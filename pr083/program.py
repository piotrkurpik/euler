#!/usr/bin/python3
from pr081.program import smallest_positive, load_matrix


def add_zeros_around(matrix):
    new_matrix = [[0]*(len(matrix[0])+2)]
    for line in matrix:
        new_matrix.append([0] + line + [0])
    new_matrix.append([0]*(len(matrix[0])+2))
    return new_matrix


def find_four_ways_path(matrix):
    matrix = add_zeros_around(matrix)
    init_matrix = [line[1:-1] for line in matrix[1:-1]]
    for i, line in enumerate(matrix[1:-1]):
        for j, value in enumerate(line[1:-1]):
            matrix[i+1][j+1] = init_matrix[i][j] + smallest_positive(
                matrix[i+1][j],
                matrix[i][j+1],
            )
    old_solution = 0
    new_solution = matrix[-2][-2]
    while old_solution != new_solution:
        old_solution = matrix[-2][-2]
        for i, line in enumerate(matrix[1:-1]):
            for j, value in enumerate(line[1:-1]):
                if i + j != 0:
                    matrix[i+1][j+1] = init_matrix[i][j] + smallest_positive(
                        matrix[i+1][j],
                        matrix[i+1][j+2],
                        matrix[i][j+1],
                        matrix[i+2][j+1]
                    )
        new_solution = matrix[-2][-2]
    return new_solution


def main():
    return find_four_ways_path(load_matrix("matrix.txt"))


if __name__ == '__main__':
    print(main())
